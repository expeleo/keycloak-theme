<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
        ${msg("loginTitle",(realm.displayNameHtml!''))}
    <#elseif section = "form">
        <#if realm.password>
            <form id="kc-form-login" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">

                <div class="input-group">
                    <div class="">
                        <input id="username" name="username" value="${(login.username!'')}" type="text" class="form-control" placeholder="<#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if>">
                    </div>
                </div>

                <div class="input-group">
                    <div class="">
                        <input id="password" name="password" type="password" autocomplete="off" class="form-control" placeholder="${msg("password")}">
                    </div>
                </div>
                <#if realm.rememberMe && !usernameEditDisabled??>
                    <div class="checkbox">
                        <label>

                            <#if login.rememberMe??>
                                <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3" checked>
                            <#else>
                                <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3">
                            </#if>
                            <span class="checkbox-material"><span class="check"></span></span>
                            ${msg("rememberMe")}
                        </label>
                    </div>
                </#if>

                <div class="${properties.kcFormGroupClass!}">
                    <div id="kc-form-buttons">
                        <div class="${properties.kcFormButtonsWrapperClass!}">
                            <button class="btn btn-login" translate="" type="submit"">${msg("doLogIn")}</button>
                        </div>
                    </div>
                    <div id="kc-form-options">
                        <div class="${properties.kcFormOptionsWrapperClass!}">
                            <#if realm.resetPasswordAllowed>
                                <span><a href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                            </#if>
                        </div>
                    </div>

                </div>
            </form>
        </#if>
    </#if>
</@layout.registrationLayout>
